Cisco WebEx is a meeting creation Tool.
Its provides WebEx API to support meeting creation through website.
For webEx, we have to create an account for this feature.

Installation and Configuration

Webex :
- we have to create the webex account.

Install:
-Install this module.
Once you have installed the module, you need to provide the below 
configuration information in the config setting page

WebEx API Url *
WebEx ID*
WebEx Authentication Password*
WebEx Site ID*
WebEx Partner ID*
WebEx Email*

In the webex configuration page you can select the content type.
Once submit the configuration. you could see the meeting date option
and add attendee options & recurring mode for selected content type.

Meeting Date - WebEx Meeting date
Attendees - who wants to attend this meeting, we can add those email address.
webEx mail will trigger to respective attendees
Recurring Mode - Choose recurring type by daily/weekly/monthly/yearly.
